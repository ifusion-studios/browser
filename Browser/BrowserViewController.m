//
//  BrowserViewController.m
//
//  Created by Jaha Rabari on 12/04/14.
//
// Copyright (c) 2014 Jaha Rabari
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#define delegatez  (((AppDelegate *)[[UIApplication sharedApplication] delegate]))
#define b [delegatez bookmarksArray]
#define c [delegatez historyArray]
#import "BrowserViewController.h"

@implementation BrowserViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(forword:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backword:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [swipeLeft setNumberOfTouchesRequired:1];
    [swipeRight setNumberOfTouchesRequired:1];
    [_webView addGestureRecognizer:swipeLeft];
    [_webView addGestureRecognizer:swipeRight];
    
    
    
    [backButton setEnabled:NO];
    forwardButton.enabled=NO;
    tabController= [[BrowserTabView alloc] initWithTabTitles:[NSArray arrayWithObjects:@"New Tab", nil]
                                                 andDelegate:self];
    
    [self.view addSubview:tabController];
    
    
}


-(void)add:(id)sender
{
    
}
- (IBAction)newTabClicked:(id)sender {
    [tabController addTabWithTitle:nil];
    [self loadWebPageFromString:nil];
    
}
- (IBAction)showBookmark:(id)sender {
}
- (IBAction)addToBookmarks:(id)sender {
}
- (IBAction)forword:(id)sender {
    [_webView goForward];
}
- (IBAction)backword:(id)sender {
    [_webView goBack];
}

- (void) loadWebPageFromString:(NSString *)string {
    NSURL *url = [NSURL URLWithString:string];
    
    NSString *googleSearch = [string stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.google.com/search?q=%@", googleSearch]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [_webView loadRequest:request];
}


-(void)loadAddress:(NSString *)mystring{
    
    NSURL *myurl = [NSURL URLWithString:mystring];
    if(!myurl.scheme){
        myurl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", mystring]];
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:myurl];
        [_webView loadRequest:request];}
    else{
        myurl = [NSURL URLWithString:[NSString stringWithFormat:@"%@", mystring]];
        
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:myurl];
        [_webView loadRequest:request];
        
    }
    
}
- (BOOL) urlIsValid: (NSString *) url
{
    NSString *regex =
    @"^(?i)(?:(?:https?|ftp):\\/\\/)?(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:\\/[^\\s]*)?$";
    /// OR use this
    ///NSString *regex = "(http|ftp|https)://[\w-_]+(.[\w-_]+)+([\w-.,@?^=%&:/~+#]* [\w-\@?^=%&/~+#])?";
    NSPredicate *regextest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([regextest evaluateWithObject: url] == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid URL"
                                                        message:@"Enter valid URL."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    return [regextest evaluateWithObject:url];
}
-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection"
                                                    message:@"Your device is not connected to internet."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}
- (void)checkForWIFIConnection {
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    if (netStatus!=ReachableViaWiFi)
    {
        [self showAlert];
    }
}

- (IBAction)googleEntered:(UITextField *)sender {
    
    
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    if (netStatus==ReachableViaWiFi){
        [self loadWebPageFromString:_searchBar.text];
        [self textFieldDidEndEditing:_searchBar];
        _addressBar.text=[NSString stringWithFormat: @"http://www.google.com/search?q=%@",_searchBar.text] ;
    }else {[self checkForWIFIConnection];self.addressBar.text=NULL;}
    
    
}
-(IBAction)addressEntered:(UITextField *)sender {
    Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    if (netStatus==ReachableViaWiFi && [self urlIsValid:_addressBar.text]){
        
        
        
        [self loadAddress:_addressBar.text];
        
        
//        [c addObject:_addressBar.text];
        
    }else [self checkForWIFIConnection];
    [sender resignFirstResponder];
    _searchBar.text =NULL;
    
}
#pragma mark - UIWebView Delegate
#pragma mark -

- (void)webViewDidFinishLoad:(UIWebView *)webview
{
    [tabController.delegate browserTabView:tabController shouldChangeTitle:@"WEB"];
    
    if ([_webView canGoBack]) {
        [backButton setEnabled:YES];
        
    } else [backButton setEnabled:NO];
    
    if ([_webView canGoForward]) {
        [forwardButton setEnabled:YES];
        
    } else forwardButton.enabled=NO;
}
-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        
        NSURL *URL=[request URL];
        
        [self checkForWIFIConnection];
        if ([URL scheme] ) {
            Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
            
            NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
            
            if (netStatus==ReachableViaWiFi ){
                _addressBar.text=URL.absoluteString;
                _searchBar.text=NULL;
                [_webView loadRequest:request];
                
//                [c addObject:URL.absoluteString];
            }
            
            
            
            
        }
        return NO;
    }
    return YES;
    
}
#pragma mark - UITextField Deleate
#pragma mark -
-(void)textFieldDidEndEditing :(UITextField *)textField
{
    
    
    if (textField.text){
        
        if (textField==_searchBar){
            
            NSString *z=[NSString stringWithFormat:@"http://www.google.com/search?q=%@",textField.text] ;
//            [c addObject:z];
            
        }
    }
}

#pragma mark -
#pragma mark BrowserTabViewDelegate
-(void)BrowserTabView:(BrowserTabView *)browserTabView didSelecedAtIndex:(NSUInteger)index
{
    NSLog(@"BrowserTabView select Tab at index:  %d",index);
    
}

-(void)BrowserTabView:(BrowserTabView *)browserTabView willRemoveTabAtIndex:(NSUInteger)index {
    NSLog(@"BrowserTabView WILL Remove Tab at index:  %d",index);
    
}

-(void)BrowserTabView:(BrowserTabView *)browserTabView didRemoveTabAtIndex:(NSUInteger)index{
    NSLog(@"BrowserTabView did Remove Tab at index:  %d",index);
}
-(void)BrowserTabView:(BrowserTabView *)browserTabView exchangeTabAtIndex:(NSUInteger)fromIndex withTabAtIndex:(NSUInteger)toIndex{
    
    NSLog(@"BrowserTabView exchange Tab  at index:  %d with Tab at index :%d ",fromIndex,toIndex);
}

- (BOOL)browserTabView:(BrowserTabView *)tabView shouldChangeTitle:(NSString *)title {
    if (title.length) {
        return YES;
    };
    return NO;
}
@end
